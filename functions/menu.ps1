# Import functions
. ".\functions\menu\menuResourceGroup.ps1"
. ".\functions\menu\menuStorageAccount.ps1"
. ".\functions\menu\menuStorageShare.ps1"
. ".\functions\menu\menuDrive.ps1"

# Main menu
function menu {
    param (
        [string]$title = 'File Share with Azure'
    )

    do {

        Write-Host "================ $title ================"
        Write-Host "1: Press '1' to access the resource groups."
        Write-Host "2: Press '2' to access the storage accounts."
        Write-Host "3: Press '3' to access the storage shares."
        Write-Host "4: Press '4' to access the network drives."
        Write-Host "Q: Press 'Q' to to quit."

        $choice = Read-Host "Please make a choice"
        Write-Host "`n"

        switch ($choice) {
            '1' { 

                menuResourceGroup

            } '2' {

                menuStorageAccount

            } '3' {

                menuStorageShare

            } '4' {

                menuDrive

            }
        }
        
    } until ($choice -eq 'Q')
    
}