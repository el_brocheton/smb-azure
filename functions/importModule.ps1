# Import an existing module
function importModule {

    try {

        Clear-Host
        
        Import-Module Az.Accounts -ErrorAction Stop
        Write-Host "Module Az.Accounts successfully imported."

        Import-Module Az.Storage -ErrorAction Stop
        Write-Host "Module Az.Storage successfully imported."

        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Impossible to import modules:" -ForegroundColor Red -BackgroundColor Black

        foreach ($e in $error) {

            Write-Host "$($e)" -ForegroundColor Red -BackgroundColor Black

        }

        Exit
    }
    
}