# Import functions
. ".\functions\get\getStorageAccounts.ps1"
. ".\functions\create\createStorageAccount.ps1"
. ".\functions\delete\deleteStorageAccount.ps1"

# Storage accounts menu
function menuStorageAccount {
    param (
        [string]$title = 'Storage account with Azure'
    )

    do {

        Write-Host "================ $title ================"
        Write-Host "1: Press '1' to list storage accounts."
        Write-Host "2: Press '2' to create a storage account."
        Write-Host "1: Press '3' to remove a storage account."
        Write-Host "Q: Press 'Q' to to quit."

        $choice = Read-Host "Please make a choice"
        Write-Host "`n"

        switch ($choice) {
            '1' {

                getStorageAccounts

            } '2' {

                $name = Read-Host "Name of the storage account to be created"
                $location = Read-Host "Location of the storage account to be created"
                $resourceGroupName = Read-Host "Name of the resource group that will contain the storage account"
                Write-Host "`n"

                createStorageAccount $name $location $resourceGroupName

            } '3' {

                $name = Read-Host "Name of the storage account to be deleted (all contained things will be deleted)"
                $resourceGroupName = Read-Host "Name of the resource group that will contain the storage account"
                Write-Host "`n"

                deleteStorageAccount $name $resourceGroupName

            }
        }

        
    } until ($choice -eq 'Q')
    
}