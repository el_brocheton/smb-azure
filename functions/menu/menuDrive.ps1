# Import functions
. ".\functions\get\getDrives.ps1"
. ".\functions\get\getAccountKey.ps1"
. ".\functions\create\createDrive.ps1"
. ".\functions\delete\deleteDrive.ps1"

# Drives menu
function menuDrive {
    param (
        [string]$title = 'Mount network drive with Powershell'
    )

    do {

        Write-Host "================ $title ================"
        Write-Host "1: Press '1' to list drives."
        Write-Host "2: Press '2' to create a network drive."
        Write-Host "1: Press '3' to remove a network drive."
        Write-Host "Q: Press 'Q' to to quit."

        $choice = Read-Host "Please make a choice"
        Write-Host "`n"

        switch ($choice) {
            '1' {

                getDrives

            } '2' {

                $name = Read-Host "Letter of the network drive to be created"
                $storageShareName = Read-Host "Name of the storage share that will mounted"
                $storageAccountName = Read-Host "Name of the storage account that will contain the storage share"
                $resourceGroupName = Read-Host "Name of the resource group that will contain the storage account"
                Write-Host "`n"

                $credential = getAccountKey $resourceGroupName $storageAccountName
                createDrive $name $storageAccountName $storageShareName $credential

            } '3' {

                $name = Read-Host "Letter of the network drive to be deleted"
                Write-Host "`n"

                deleteDrive $name

            }
        }

        
    } until ($choice -eq 'Q')
    
}