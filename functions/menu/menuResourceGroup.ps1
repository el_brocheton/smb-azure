# Import functions
. ".\functions\get\getResourceGroups.ps1"
. ".\functions\create\createResourceGroup.ps1"
. ".\functions\delete\deleteResourceGroup.ps1"

# Resource groups menu
function menuResourceGroup {
    param (
        [string]$title = 'Resource group with Azure'
    )

    do {

        Write-Host "================ $title ================"
        Write-Host "1: Press '1' to list resource groups."
        Write-Host "2: Press '2' to create a resource group."
        Write-Host "1: Press '3' to remove a resource group."
        Write-Host "Q: Press 'Q' to to quit."

        $choice = Read-Host "Please make a choice"
        Write-Host "`n"

        switch ($choice) {
            '1' {

                getResourceGroups

            } '2' {

                $name = Read-Host "Name of the resource group to be created"
                $location = Read-Host "Location of the resource group to be created"
                Write-Host "`n"

                createResourceGroup $name $location

            } '3' {

                $name = Read-Host "Name of the resource group to be deleted (all contained resources will be deleted)"
                Write-Host "`n"

                deleteResourceGroup $name

            }
        }

        
    } until ($choice -eq 'Q')
    
}