# Import functions
. ".\functions\get\getStorageAccountContext.ps1"
. ".\functions\get\getStorageShares.ps1"
. ".\functions\create\createStorageShare.ps1"
. ".\functions\delete\deleteStorageShare.ps1"

# Storage shares menu
function menuStorageShare {
    param (
        [string]$title = 'Storage share with Azure'
    )

    do {

        Write-Host "================ $title ================"
        Write-Host "1: Press '1' to list storage shares."
        Write-Host "2: Press '2' to create a storage share."
        Write-Host "1: Press '3' to remove a storage share."
        Write-Host "Q: Press 'Q' to to quit."

        $choice = Read-Host "Please make a choice"
        Write-Host "`n"

        switch ($choice) {
            '1' {

                $storageAccountName = Read-Host "Name of the storage account that contains the storage shares"
                $resourceGroupName = Read-Host "Name of the resource group that contains the storage account"

                $storageAccountContext = getStorageAccountContext $storageAccountName $resourceGroupName
                getStorageShares $storageAccountContext

            } '2' {

                $name = Read-Host "Name of the storage share to be created"
                $storageAccountName = Read-Host "Name of the storage account that will contain the storage share"
                $resourceGroupName = Read-Host "Name of the resource group that will contain the storage account"
                Write-Host "`n"

                $storageAccountContext = getStorageAccountContext $storageAccountName $resourceGroupName
                createStorageShare $name $storageAccountContext

            } '3' {

                $name = Read-Host "Name of the storage share to be deleted (all contained things will be deleted)"
                $storageAccountName = Read-Host "Name of the storage account that will contain the storage share"
                $resourceGroupName = Read-Host "Name of the resource group that will contain the storage account"
                Write-Host "`n"

                $storageAccountContext = getStorageAccountContext $storageAccountName $resourceGroupName
                deleteStorageShare $name $storageAccountContext

            }
        }

        
    } until ($choice -eq 'Q')
    
}