# Disconnect from Azure account
function disconnectAzAccount {
    
    try {
        
        Disconnect-AzAccount -ErrorAction Stop | Out-Null
        Write-Host "Successful logout."
        
    }
    catch {
        
        Write-Host "Unsuccessful logout: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        
    }
    
}