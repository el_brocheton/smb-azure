# Connect to Azure account
function connectAzAccount {

    try {
        
        Connect-AzAccount -ErrorAction Stop
        Write-Host "Successful login."
        Write-Host "`n"
        
    }
    catch {
        
        Write-Host "Unsuccessful login: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Exit
        
    }
    
}