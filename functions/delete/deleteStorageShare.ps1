# Delete a storage share
function deleteStorageShare {
    param (
        [string]$name,
        $storageAccountContext
    )

    try {
        
        Remove-AzStorageShare -Name $name -Context $storageAccountContext -ErrorAction Stop | Out-Null
        
        Clear-Host
        Write-Host "The $name storage share has been deleted."
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to delete the $name storage share: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}