# Delete a resource group
function deleteResourceGroup {
    param (
        [string]$name
    )

    try {
        
        Remove-AzResourceGroup -Name $name -ErrorAction Stop | Out-Null
        
        Clear-Host
        Write-Host "The $name group resource has been deleted."
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to delete the $name resource group: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}