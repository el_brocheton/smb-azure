# Delete a storage account
function deleteStorageAccount {
    param (
        [string]$name,
        [string]$resourceGroupName
    )

    try {
        
        Remove-AzStorageAccount -Name $name -ResourceGroupName $resourceGroupName -ErrorAction Stop | Out-Null
        
        Clear-Host
        Write-Host "The $name storage account has been deleted."
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to delete the $name storage account: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}