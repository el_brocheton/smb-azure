# Delete a network drive
function deleteDrive {
    param (
        [string]$name
    )

    try {
        
        Remove-PSDrive -Name $name -ErrorAction Stop | Out-Null
        
        Clear-Host
        Write-Host "The $name network drive has been deleted."
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to delete the $name network drive: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}