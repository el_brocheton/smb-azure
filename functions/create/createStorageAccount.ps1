# Create a storage account in an existing resource group
function createStorageAccount {
    param (
        [string]$name,
        [string]$location,
        [string]$resourceGroupName
    )
    
    try {
        
        New-AzStorageAccount -ResourceGroupName $resourceGroupName `
            -Name $name `
            -Location $location `
            -SkuName Standard_LRS `
            -Kind StorageV2 `
            -ErrorAction Stop `
            | Out-Null

        Clear-Host
        Write-Host "The $name storage account has been created in the $resourceGroupName resource group."
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to create the storage account: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }

}