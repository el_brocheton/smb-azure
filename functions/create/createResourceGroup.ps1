# Create a resource group
function createResourceGroup {
    param (
        [string]$name,
        [string]$location
    )
    
    try {
        
        New-AzResourceGroup -Name $name -Location $location -ErrorAction Stop | Out-Null

        Clear-Host
        Write-Host "The $name resource group has been created."
        Write-Host "`n"

    }
    catch {
        
        Clear-Host
        Write-Host "Unable to create the ressource group: $($error[0])" -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }

}