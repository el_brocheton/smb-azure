# Create a network drive from a storage share
function createDrive {
    param (
        [string]$name,
        [string]$storageAccountName,
        [string]$storageShareName,
        $credential
    )

    try {

        New-PSDrive -Name $name `
            -PSProvider FileSystem `
            -Root "\\$storageAccountName.file.core.windows.net\$storageShareName" `
            -Credential $credential `
            -Scope Global `
            -Persist `
            -ErrorAction Stop `
            | Out-Null

        Clear-Host
        Write-Host "The $name drive has been mounted correctly."
        Write-Host "`n"
        
    }
    catch {

        Clear-Host
        Write-Host "Unable to mount the network drive: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}