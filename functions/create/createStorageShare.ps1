# Create a file share in an existing storage account
function createStorageShare {
    param (
        [string]$name,
        $storageAccountContext
    )

    try {
        
        New-AzStorageShare -Context $storageAccountContext -Name $name -ErrorAction Stop | Out-Null
        
        Clear-Host
        Write-Host "The $name storage sharing has been created."
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to create the storage share: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"

    }
    
}