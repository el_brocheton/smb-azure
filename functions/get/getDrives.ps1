# Get all drives
function getDrives {

    try {
        
        Clear-Host
        Get-PSDrive -PSProvider FileSystem -ErrorAction Stop
        Write-Host "`n"
        
    }
    catch {

        Clear-Host
        Write-Host "Unable to list the drives: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"

    }
    
}