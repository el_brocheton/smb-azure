# Get a storage account key
function getAccountKey {
    param (
        [string]$resourceGroupName,
        [string]$storageAccountName
    )

    try {
        
        $storageKey = (Get-AzStorageAccountKey -ResourceGroupName $resourceGroupName -Name $storageAccountName -ErrorAction Stop).Value[0]
        [SecureString]$accountKey = ConvertTo-SecureString -String $storageKey -AsPlainText -Force -ErrorAction Stop
        $credential = New-Object -TypeName System.Management.Automation.PSCredential `
            -ArgumentList "Azure\$storageAccountName", $accountKey `
            -ErrorAction Stop
        return $credential

    }
    catch {

        Clear-Host
        Write-Host "Impossible to get the storage account key: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        return Break

    }
    
}