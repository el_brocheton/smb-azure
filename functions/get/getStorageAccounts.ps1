# Get all storage accounts
function getStorageAccounts {

    try {
        
        Clear-Host
        Get-AzStorageAccount -ErrorAction Stop | Select-Object StorageAccountName, ResourceGroupName, Location | Sort-Object ResourceGroupName
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to list the storage accounts: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}