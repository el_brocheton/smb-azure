# Get a storage account
function getStorageAccountContext {
    param (
        [string]$name,
        [string]$resourceGroupName
    )

    try {
        
        $storageAccount = Get-AzStorageAccount -Name $name -ResourceGroupName $resourceGroupName -ErrorAction Stop
        return $storageAccount.Context

    }
    catch {
        
        Clear-Host
        Write-Host "Impossible to get the informations of the storage account: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        return Break

    }
    
}