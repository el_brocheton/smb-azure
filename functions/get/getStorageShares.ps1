# Get all storage shares of a given storage account
function getStorageShares {
    param (
        $storageAccountContext
    )

    try {
        
        Clear-Host
        Get-AzStorageShare -Context $storageAccountContext -ErrorAction Stop | Select-Object Name, StorageUri
        Write-Host "`n"

    }
    catch {

        Clear-Host
        Write-Host "Unable to list the storage shares: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"
        
    }
    
}