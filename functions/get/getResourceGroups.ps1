# Get all resource groups
function getResourceGroups {

    try {
        
        Clear-Host
        Get-AzResourceGroup -ErrorAction Stop | Select-Object ResourceGroupName, Location
        Write-Host "`n"
        
    }
    catch {

        Clear-Host
        Write-Host "Unable to list the resource groups: $($error[0])." -ForegroundColor Red -BackgroundColor Black
        Write-Host "`n"

    }
    
}