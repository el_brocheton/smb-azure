# Script to deploy SMB sharing on Azure
# version 1.0
# Author : Lucas BROCHETON

# Import functions
. ".\functions\importModule.ps1"
. ".\functions\azAccount\connectAzAccount.ps1"
. ".\functions\menu.ps1"
. ".\functions\azAccount\disconnectAzAccount.ps1"

# Import Azure module
importModule

# Azure connection
connectAzAccount

# Show menu
menu

# Azure disconnection
disconnectAzAccount