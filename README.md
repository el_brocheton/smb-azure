# SMB with Azure

## Summary

- [Introduction](#introduction)
- [Modules](#modules)
- [Azure account](#azure-account)
- [Main menu](#main-menu)
- [Resource groups](#resource-groups)
- [Storage accounts](#storage-accounts)
- [Storage shares](#storage-shares)
- [Network drives on Windows](#network-drives-on-windows)

## Introduction

Two associations need to move their NAS storage to the cloud. Here, we chose Microsoft Azure to do so.

So here is a script to list, create and delete resource groups, storage accounts, storage shares and network drives.

> The script must be executed without administrative rights.

## Modules

In order to be able to manage Azure, we need to import some Powershell modules:

- Az.Accounts
- Az.Storage

## Azure account

Of course to be able to do what you want, you have to connect to your Azure account before starting.

> For your information, you are logged out of Azure when you exit the script.

## Main menu

From the main menu, you can access all the dedicated menus.

## Resource groups

It is possible to list, create and delete resource groups from a dedicated menu.

## Storage accounts

It is possible to list, create and delete storage accounts from a dedicated menu.

## Storage shares

It is possible to list, create and delete storage shares from a dedicated menu.

## Network drives on Windows

It is possible to list, create and delete network shares on Windows from a dedicated menu.
